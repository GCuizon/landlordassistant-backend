package com.landlordassistant;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.landlordassistant.dao.ApartmentDAO;
import com.landlordassistant.dao.TenantDAO;
import com.landlordassistant.data.AptStatus;

import lombok.extern.log4j.Log4j2;

@RunWith(SpringRunner.class)
@SpringBootTest
@Log4j2
public class LandlordAssistantApplicationTests {

	@Autowired
	ApartmentDAO apartmentDAO;

	@Autowired
	TenantDAO tenantDAO;

	@Test
	public void testApartmentDAO() {
		apartmentDAO.findAll().stream().forEach(apartment -> {
			log.info("Apartment ID#:{} => Desc: {}; Status: {}", apartment.getId(), apartment.getDesc(), apartment.getStatus().getDescription());
		});
	}

	@Test
	public void testGetVacantApts(){
		apartmentDAO.getApartmentsByStatus(AptStatus.OCC.name()).stream().forEach(apartment -> {
			log.info("Apartment ID#:{} => Desc: {}; Status: {}", apartment.getId(), apartment.getDesc(), apartment.getStatus().getDescription());
		});
	}

}
