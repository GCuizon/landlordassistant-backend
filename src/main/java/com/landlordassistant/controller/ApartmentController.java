package com.landlordassistant.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.landlordassistant.data.Apartment;
import com.landlordassistant.service.ApartmentService;

@RestController
@RequestMapping("/apartments")
@CrossOrigin("*")
public class ApartmentController {
	
	@Resource
	private ApartmentService apartmentService;
	
	@GetMapping("/getAll")
	public List<Apartment> getApartments(){
		return apartmentService.getAllApartments();
	}
}
