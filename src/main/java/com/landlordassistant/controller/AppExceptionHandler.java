package com.landlordassistant.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@RestController
public class AppExceptionHandler {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		ServerResponse serverResponse = new ServerResponse(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<Object>(serverResponse, HttpStatus.INTERNAL_SERVER_ERROR);

	}
}
