package com.landlordassistant.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.landlordassistant.dao.ApartmentDAO;
import com.landlordassistant.data.Apartment;

@Service("apartmentService")
public class ApartmentService {

	@Autowired
	private ApartmentDAO apartmentDAO;

	public List<Apartment> getAllApartments() {
		return apartmentDAO.findAll();
	}

	public Apartment getApartmentDetails(Long id) {
		return apartmentDAO.getOne(id);
	}

}
