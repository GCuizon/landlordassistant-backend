package com.landlordassistant.service;

import com.landlordassistant.data.TenantDO;

import java.util.List;

public interface TenantService {
    public List<TenantDO> getAllTenants();
    public List<TenantDO> getAllTenantsInApt(Long aptId);
    public TenantDO getTenantById(Long tenantId);
}
