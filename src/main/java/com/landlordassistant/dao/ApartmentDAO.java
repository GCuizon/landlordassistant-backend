package com.landlordassistant.dao;

import com.landlordassistant.data.Apartment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ApartmentDAO extends JpaRepository<Apartment, Long> {
    @Query(value = "SELECT * FROM Apartment WHERE status = :status", nativeQuery = true)
    public List<Apartment> getApartmentsByStatus(String status);
}
