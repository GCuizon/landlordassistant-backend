package com.landlordassistant.dao;

import com.landlordassistant.data.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TenantDAO extends JpaRepository<Tenant, Long> {}
