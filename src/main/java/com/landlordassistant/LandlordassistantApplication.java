package com.landlordassistant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LandlordassistantApplication {

	public static void main(String[] args) {
		SpringApplication.run(LandlordassistantApplication.class, args);
	}

}
