package com.landlordassistant.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;

@Entity
@Data
public class Apartment {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String desc;

	@Column
	@Enumerated(EnumType.STRING)
	private AptStatus status;
	
	/*@OneToMany(mappedBy = "apartment", cascade = CascadeType.REMOVE)
	private List<Tenant> tenants;
	 */
}
 