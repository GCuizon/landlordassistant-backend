package com.landlordassistant.data;

import lombok.Getter;

@Getter
public enum TenantStatus {
    ACT("Active"),
    INACT("Inactive"),
    BAN("Banned");

    private String description;

    TenantStatus(String description){
        this.description = description;
    }
}
