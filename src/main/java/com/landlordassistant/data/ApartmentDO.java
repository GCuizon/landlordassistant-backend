package com.landlordassistant.data;

import lombok.Data;

import java.util.List;

@Data
public class ApartmentDO {
    private Long id;
    private List<Long> tenantIds;
    private String desc;
    private AptStatus status;
}
