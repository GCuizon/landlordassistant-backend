package com.landlordassistant.data;

import lombok.Data;

@Data
public class TenantDO {
    private Long id;
    private Long aptId;
    private String fistName;
    private String lastName;
    private TenantStatus status;
}
