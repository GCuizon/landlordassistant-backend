package com.landlordassistant.data;

import javax.persistence.*;

import lombok.Data;

@Entity
@Data
public class Tenant {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column
	private String fistName;

	@Column
	private String lastName;

	@Column
	@Enumerated(EnumType.STRING)
	private TenantStatus status;
	
	/*@ManyToOne
    @JoinColumn(name = "APARTMENT_ID")
	private Apartment apartment;
	*/
}
