package com.landlordassistant.data;

import lombok.Getter;

@Getter
public enum AptStatus {
    OCC("Occupied"),
    VAC("Vacant");

    private String description;
    AptStatus(String description){
        this.description = description;
    }
}
